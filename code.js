

let tabs = document.querySelector('.tabs');

let tabsContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', (event) =>{
    const li = event.target.closest('li');

    let active = document.querySelector(".active");

    if(active){
        active.classList.remove('active');
        findeContent(li)
    }
    li.classList.add('active');
    
    });

    function findeContent(li){
        for (let i of tabsContent.children) {
        if (i.dataset.tab === li.dataset.tab) {
            li.classList.remove('active');
            i.classList.add('active');     
        }else{
            li.classList.add('active');
            i.classList.remove('active');
        }
        }
    }